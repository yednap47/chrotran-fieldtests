function K2perm(K)
  #  K [m/d]
  #  perm [m^2]  
  rhow = 998.2; # density H20, [kg/m^3]
  mu = 1.002e-3; # dynamic viscocity H2O, [kg/m/s] (i.e. [Pa s])
  g = 9.80665; # m/s^2
  k = K/86400/rhow/g*mu; # m^2 from m/d
  return k
end

function h2p(head,rho_H2O,z)
  g = 9.80665; # m/s
  pressure = (head-z)*rho_H2O*g
  return pressure
end

# HYDRAULIC CONDUCTIVITY: "use 20 m/d"
k=K2perm(20)
@printf("permeability = %0.5e m^2\n",k)

# HYDRAULIC GRADIENT: 0.0014 m/m
# 101325 Pa = 1 atm = 10.35091 m H2O
# 1000 m h20 = 9806806 Pa
# 1001 m h20 = 9816613 Pa

#hgrad = 0.0014 # m/m
hgrad = 0.01 # m/m

deltax = 141.0 # m, rotate_noRefine
head_drop = deltax*hgrad
rho_H2O = 997.16 # kg/m^3
z = 0
head_west_m = 1000.0 # m
head_east_m = head_west_m - head_drop
head_west_pa = h2p(head_west_m,rho_H2O,z)
head_east_pa = h2p(head_east_m,rho_H2O,z)

@printf("head west = %0.6e Pa\n",head_west_pa)
@printf("head east = %0.6e Pa\n",head_east_pa)

# PUMPING rate
# Convert gpm to kg/s
Q_gpm = 100 # gallons/min
Q_lpm = Q_gpm * 3.78541
Q_m3ps = Q_lpm/1000/60
Q_kgps = Q_m3ps*rho_H2O
Q_m3pd = Q_m3ps*86400 
@printf("Mass rate = %0.6f kg/s\n",Q_kgps)
@printf("Volumetric rate = %0.2f m^3/d\n",Q_m3pd)
