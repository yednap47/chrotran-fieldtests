import Pflotran

# useage
# mads_syntheticdata.jl basedir simbasename obsn myvar obsname
# e.g. 
# julia mads_syntheticdata.jl chrotran-geo report-geo 6 CrO4-- obs1

# general info
basedir = ARGS[1] # simulation directory
simbasename = ARGS[2] # simulation name
obsn = ARGS[3] # observation file number
myvar = ["Total $(ARGS[4]) [M] $(ARGS[5])"] # variable name

obsdatafname = joinpath(basedir,"$(simbasename)-obs-$obsn.tec")
results = Pflotran.readObsDataset(obsdatafname,myvar)

obstimes = results[:,1]
targets = results[:,2]

if !isdir("mads-targets")
    mkdir("mads-targets")
end 

outfile = open(joinpath(".","mads-targets","$(simbasename)-targets.txt"), "w")

for i in 1:length(obstimes)
     write(outfile, "- obs_t$(i): ")
     @printf(outfile, "{target: %0.5e, ", targets[i])
     @printf(outfile, "weight: 100.0, ")
     @printf(outfile, "time: %.2f}\n", obstimes[i])
end

close(outfile)
