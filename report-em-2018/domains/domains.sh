#!/bin/bash

./sgsim rand_600x150_corr10_sig1.par
./sgsim rand_600x150_corr10_sig1_v2.par
./sgsim rand_100x50_corr10_sig1.par
julia coordinates_noRefine.jl
julia pflotran_domain.jl rand_600x150_corr10_sig1 600 150 20
julia pflotran_domain.jl rand_600x150_corr10_sig1_v2 600 150 20
julia pflotran_domain.jl rand_100x50_corr10_sig1 100 50 1
