using HDF5
using PyPlot
plt = PyPlot
using PyCall
import Pflotran
pfl = Pflotran

@pyimport numpy as np

# USAGE:
# julia pflotran_domain.jl [sgsim data file] [nx] [ny] [K, m/d]
# e.g.
# julia pflotran_domain.jl rand_100x50_corr10_sig1 100 50 1

## User Data
fbasename = ARGS[1]

# domain info
nx = parse(Int,ARGS[2])
ny = parse(Int,ARGS[3])
nz = 1
dx = 1
dz = 1
nhead = 3 # numer of headers in reading file

mu_K = parse(Float64,ARGS[4]) # [m/d] desired mean hydraulic conductivity of K field
mu_k = pfl.K2perm(mu_K) # [m/d] desired mean hydraulic conductivity of K field
sigma_lnK = 1.0 # desired standard deviation of lnK field

# convert Kbar (log-normal) to mu_lnk (normal) HARI METHOD
mu_lnK = log(mu_K)-sigma_lnK^2/2

# open file
fname=("$(fbasename).dat")
myfile = open(fname)
temp = readlines(myfile)
close(myfile)
raw = map((x) -> parse(Float64,x),temp[4:end])
ndata = length(raw)

X = zeros(ndata,1) # hydraulic conductivity vector following normal dis
K = zeros(ndata,1) # hydraulic conductivity vector following normal dis
for i in 1:ndata
	# transform from normal to lognormal
	X[i] = raw[i]+mu_lnK
	K[i] = exp(X[i])
end

perm = pfl.K2perm(K)

# myMat = reshape(k(:,i),nx,nz)

## ========================================================================== ##
## Plot results
## ========================================================================== ##
myMat = reshape(perm,nx,ny)
subplot(111,aspect="equal")
pcolor(transpose(log10.(myMat)))
colorbar()
xlim(0,nx)
ylim(0,ny)

## ========================================================================== ##
## Coordinates of cell centers
## ========================================================================== ##
ivec = range(1,1,nx)
jvec = range(1,1,ny)
kvec = range(1,1,nz)
cellIndex = Array{Int}(nx*ny*nz,4)
for i in ivec
	for j in jvec
		for k in kvec
			index = i + (j-1)*nx + (k-1)*nx*ny
			cellIndex[index,1]=index
			cellIndex[index,2]=i
			cellIndex[index,3]=j
			cellIndex[index,4]=k
		end
	end
end

## ========================================================================== ##
## Write to h5 file
## ========================================================================== ##
htag = "$(fbasename)_$(Int(mu_K))mpd.h5"
fid = h5open(htag,"w")
write(fid, "Cell Ids", cellIndex[:,1])
write(fid, "Permeability", perm[:])  # alternatively, say "@write file A"
close(fid)

savefig("$(fbasename)_$(Int(mu_K))mpd.png")
close()

println("Field $(fbasename) parameters:")
println("mu_k = $(mean(perm[:]))")
println("mu_K = $(mean(K[:]))")
println("sigma_lnK = $(sqrt(var(log.(K[:]))))")
println()
