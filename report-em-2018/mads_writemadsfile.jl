import ExcelReaders
xlr = ExcelReaders
import DataFrames

#------------------------------------------------------------------------------
# Initialize
#------------------------------------------------------------------------------
# Useage
# julia mads_writemadsfile.jl basedir simbasename
# e.g.
# julia mads_writemadsfile.jl chrotran-rdx report-rdx

# general info
basedir = ARGS[1]
simbasename = ARGS[2]

# Default parameters for writing MADS file
paramfilename = "mads_parameters.xlsx"
jcommand = "read_data.jl"
soltype = "external"
startover = "true"

#------------------------------------------------------------------------------
# The code
#------------------------------------------------------------------------------
# Get observations
targetsf = open(joinpath("mads-targets","$(simbasename)-targets.txt"), "r")
targets = readlines(targetsf)
close(targetsf)

# write the mads file
f = xlr.openxl(paramfilename)
paraminfo = xlr.readxlsheet(paramfilename, simbasename)

if !isdir(joinpath(basedir,"sensitivity"))
    mkdir(joinpath(basedir,"sensitivity"))
end 

outfile = open(joinpath(basedir,"sensitivity","$(simbasename).mads"), "w")
println(outfile,"Julia command: $(jcommand)")

println(outfile,"Observations:")
for target in targets
     println(outfile, target)
end

println(outfile,"Parameters:")
for i in 1:length(paraminfo[:,1])
    write(outfile, "- log_$(paraminfo[i,1]): ")
    @printf(outfile, "{init: %0.4f, ", paraminfo[i,3])
    @printf(outfile, "min: %0.4f, ", paraminfo[i,4])
    @printf(outfile, "max: %0.4f, ", paraminfo[i,5])
    write(outfile, "type: opt}\n")
    println(outfile, "- $(paraminfo[i,1]): {exp: \"10^log_$(paraminfo[i,1])\"}")
end

# Finish writing mads file
println(outfile,"Solution: $(soltype)")
println(outfile,"Templates:")
println(outfile,"- tmp1: {tpl: $(simbasename).in.tpl, write: $(simbasename).in}")
println(outfile,"Restart: $(startover)")
close(outfile)
println("finished writing mads file for $basedir")
