import Pflotran
import PyPlot
plt = PyPlot
using LaTeXStrings

# Initialize
mysize = 9 # font setting
obsnames = ["obs1"] # observation name
nobsfile = [6,6,6,6] # number tagged onto the end of observation file name
mycol = ["blue","red","green","brown"] # color setting
varname = "Total CrO4-- [M]"

# # first plot the bio simulations
# basedir = "chrotran-bio/breakthrough"
# simnames = ["ct1di","ct1dx","ct1xi","ct1xx"]
# mylabels = [L"\mathrm{direct,\, ethanol}",
#             L"\mathrm{direct,\, no\, ethanol}",
#             L"\mathrm{no\, direct,\, ethanol}",
#             L"\mathrm{no\, direct,\, no\, ethanol}"]

# alternate first plot the bio simulations
basedir = "chrotran-bio/breakthrough"
# simnames = ["c1","c2","c3","c4"]
simnames = ["c1","c2","c3"]
mylabels = [L"\mathrm{10^{-1}\, M\, molasses}",
           L"\mathrm{10^{-2}\, M\, molasses}",
           L"\mathrm{10^{-3}\, M\, molasses}",
        #    L"\mathrm{10^{-4}\, M\, molasses}",
           ]

f, ax = plt.subplots(figsize=(6.5,2.7))
for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["$varname $(obsnames[j])"]
        results = Pflotran.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->Pflotran.M2ppb(x,51.9961),results[:,2])
        line = ax[:plot](x/365, y1,label=mylabels[i],color = mycol[i],ls="--",dashes=(5, 2))
        # line[:set_dashes](2)
    end
end

# now plot the geo simulations
basedir = "chrotran-geo/breakthrough"
# simnames = ["c1","c2","c3","c4"]
simnames = ["c1","c2","c3"]
mylabels = [L"\mathrm{10^{-1}\, M\, dithionite}",
           L"\mathrm{10^{-2}\, M\, dithionite}",
           L"\mathrm{10^{-3}\, M\, dithionite}",
        #    L"\mathrm{10^{-4}\, M\, dithionite}",
           ]
nobsfile = [6,6,6,6]

for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["$varname $(obsnames[j])"]
        results = Pflotran.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->Pflotran.M2ppb(x,51.9961),results[:,2])
        # y1[y1.>1000]=1000
        line = ax[:plot](x/365, y1,label=mylabels[i],color = mycol[i],ls="-")
    end
end

# now plot control (no reaction)
filename = "chrotran-bio/breakthrough/norxn-obs-6.tec"
for j in 1:length(obsnames)
	myvar = ["$varname $(obsnames[j])"]
	results = Pflotran.readObsDataset(filename,myvar)
	x = results[:,1]
	y1 = map(x->Pflotran.M2ppb(x,51.9961),results[:,2])
	line = ax[:plot](x/365, y1,label=L"\mathrm{Control\, (no\, reaction)}",color = "k",ls="--")
end

new_position = [0.1,0.15,.67,.8] # Position Method 2
ax[:set_position](new_position) # Position Method 2: Change the size and position of the axis
ax[:tick_params](labelsize=mysize)
ax[:set_xlabel]("Time [year]",size=mysize)
ax[:set_ylabel]("Cr(VI) [ppb]",size=mysize)
ax[:set_xlim]((0,1))

# plt.legend(loc=0,frameon=false,fontsize=mysize-2)
ax[:legend](loc=4, bbox_to_anchor=(1.34, 0.0),fontsize=mysize-1,handlelength=2,frameon=false)

# tight_layout(h_pad=.1)
plt.savefig("breakthrough_biogeo.png",dpi=600)
close()
