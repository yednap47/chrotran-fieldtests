import Pflotran

basename = ARGS[1]
h5varname = ["Liquid X-Velocity [m_per_d]","Liquid Y-Velocity [m_per_d]"]
mytime = 5
phi = 0.15

filename = "$(basename).h5"
results = Pflotran.readh5_2D("$(basename).h5",h5varname,mytime)

v = (mean(results["Liquid X-Velocity [m_per_d]"])^2+mean(results["Liquid Y-Velocity [m_per_d]"])^2)^0.5
v_porewater = v/phi

println("mean porewater velocity for $basename = $v_porewater m/day")
