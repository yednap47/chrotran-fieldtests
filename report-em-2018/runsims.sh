#!/bin/bash

date
pwd

# Read arguments
for i in "$@"
do
case $i in
    -t=*|--testdir=*)
    TESTDIR="${i#*=}"
    ;;
    -f=*|--filename=*)
    FILENAME="${i#*=}"
    ;;
    -np=*|--nproc=*)
    NPROC="${i#*=}"
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
done

# User defined
PFLOTRAN_EXE=/lclscratch/sach/Programs/chrotran/release-2.0/src/pflotran/chrotran # Path for chrotran executable
MYDIR=/lclscratch/sach/chrotran-fieldtests/report-em-2018 # Base path for examples

# The code
INPUT_FILE=${FILENAME}.in
OUTPUT_FILE=${FILENAME}.log
TEC_FILE=${FILENAME}-obs-6.tec

cd ${MYDIR}/${TESTDIR}

if [ -f "${OUTPUT_FILE}" ]; then
  echo "deleting log file"
  rm -rf ${OUTPUT_FILE}
fi

if [ -f "${TEC_FILE}" ]; then
  echo "deleting tec file"
  rm -rf ${TEC_FILE}
fi

if [ "${NPROC}" -gt 1 ]
then
echo "mpirun -np ${NPROC} ${PFLOTRAN_EXE} -pflotranin ${INPUT_FILE} >& ${OUTPUT_FILE}"
mpirun -np ${NPROC} ${PFLOTRAN_EXE} -pflotranin ${INPUT_FILE}  >& ${OUTPUT_FILE}
else
echo "${PFLOTRAN_EXE} -pflotranin ${INPUT_FILE} >& ${OUTPUT_FILE}"
${PFLOTRAN_EXE} -pflotranin ${INPUT_FILE} >& ${OUTPUT_FILE}
fi
