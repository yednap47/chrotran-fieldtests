import Pflotran
pfl = Pflotran

# Constants
deltax = 600.0 # m
head_west = 1000.0 # m
pressure_west = pfl.h2p(head_west) # Pa
@printf("pressure west = %0.8e Pa\n",pressure_west)

# # METHOD 1, specify hgrad
# hgrad = 0.1 # m/m
# pressure_east = pfl.calc_pressure_east(deltax,head_west,hgrad)

# METHOD 2, specify porewater velocity, phi, and K
phi = 0.15 # -
K = 20 # m/d

# chrotran-bio
v_porewater = 1.0 # m/d
pressure_east = pfl.calc_pressure_east(deltax,head_west,v_porewater,phi,K)

print("For v_porewater = $(v_porewater), ")
@printf("pressure east = %0.8e Pa\n",pressure_east)

# chrotran-geo
v_porewater = 0.1 # m/d
pressure_east = pfl.calc_pressure_east(deltax,head_west,v_porewater,phi,K)

print("For v_porewater = $(v_porewater), ")
@printf("pressure east = %0.8e Pa\n",pressure_east)

# chrotran-rdx
deltax = 50.0 # m
hgrad = 0.05 # m/m
pressure_east = pfl.calc_pressure_east(deltax,head_west,hgrad)
print("For hgrad = $(hgrad), ")
@printf("pressure east = %0.8e Pa\n",pressure_east)
println()


# PUMPING rate
# Convert gpm to kg/s
rho_H2O = 998.21
Q_gpm = 10 # gpm
starttime = 10.0 # d
endtime = 30.0 # day
timeunits = "d"

# calculations
Q_lpm = Q_gpm * 3.78541
Q_lpd = Q_lpm * 60 * 24
Q_m3ps = Q_lpm/1000/60
Q_kgps = Q_m3ps*rho_H2O
Q_m3pd = Q_m3ps*86400 

outfile = open("pumping_condition.txt", "w")
write(outfile, "FLOW_CONDITION well\n")
write(outfile, "  TYPE\n")
write(outfile, "    RATE mass_rate\n")
write(outfile, "  /\n")
write(outfile, "  RATE list\n")
write(outfile, "    TIME_UNITS d\n")
write(outfile, "    DATA_UNITS kg/s\n")
write(outfile, "    0.0 0.0\n")
write(outfile, "    $starttime $(round(Q_kgps,4))\n")
write(outfile, "    $endtime 0.0\n")
write(outfile, "  /\n")
write(outfile, "END")
close(outfile)

println("See pumping_condition.txt for injection input block.")
println()

# Calculate MASS of remediant 
conc = 1.e-2
mw_molasses = 201.22
mw_na2s2o4 = 142.04
mw_fructose = 180.16
mass_molasses = Q_lpd * (endtime-starttime) * conc * mw_molasses / 1000 # kg
mass_na2s2o4 = Q_lpd * (endtime-starttime) * conc * mw_na2s2o4 / 1000 # kg
mass_fructose = Q_lpd * (endtime-starttime) * conc * mw_fructose / 1000 # kg

println("Total mass of amendment for injection rate of $(Q_gpm) gpm:")
println("$(mass_na2s2o4) kg sodium dithionite injected")
println("$(mass_molasses) kg molasses injected")
println("$(mass_fructose) kg fructose injected")
println("Total volume of water injected is $(Q_gpm * (endtime-starttime)*24*60) gallons")
