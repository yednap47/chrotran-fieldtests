#!/bin/bash

# Run domains/domains.sh (creates domain h5 files) 
cd domains
echo making domain h5 files
./domains.sh > domains.log
cd ..

# Run chrotran simulations
./runsims.sh -t=chrotran-bio -f=report-bio -np=16
./runsims.sh -t=chrotran-bio -f=report-bio-norxn -np=16
./runsims.sh -t=chrotran-geo -f=report-geo -np=16
./runsims.sh -t=chrotran-geo -f=report-geo-norxn -np=16
./runsims.sh -t=chrotran-rdx -f=report-rdx -np=16
./runsims.sh -t=chrotran-rdx -f=report-rdx-norxn -np=16

./runsims.sh -t=chrotran-bio/breakthrough -f=c1 -np=16
./runsims.sh -t=chrotran-bio/breakthrough -f=c2 -np=16
./runsims.sh -t=chrotran-bio/breakthrough -f=c3 -np=16
./runsims.sh -t=chrotran-bio/breakthrough -f=c4 -np=16
./runsims.sh -t=chrotran-bio/breakthrough -f=norxn -np=16

./runsims.sh -t=chrotran-geo/breakthrough -f=c1 -np=16
./runsims.sh -t=chrotran-geo/breakthrough -f=c2 -np=16
./runsims.sh -t=chrotran-geo/breakthrough -f=c3 -np=16
./runsims.sh -t=chrotran-geo/breakthrough -f=c4 -np=16
./runsims.sh -t=chrotran-geo/breakthrough -f=norxn -np=16

julia plot-obs-bio-vs-geo.jl

# Calculate stuff
julia calc_vporewater.jl chrotran-geo/report-geo
julia calc_vporewater.jl chrotran-bio/report-bio

# RDX simulation results
cd chrotran-rdx
python plot-cmap.py
julia plot-obs.jl
cd ..

# Make movies 
julia plot-movie-geo.jl
julia plot-movie-bio.jl

cd movie-geo
if [ -f movie-geo.mp4 ]; then
  rm movie-geo.mp4
fi
 
ffmpeg -f image2 -i movie-geo%d.png -vf scale="trunc(iw/2)*2:trunc(ih/2)*2" -c:v libx264 -profile:v high -pix_fmt yuv420p -g 20 -r 30 movie-geo.mp4

cd ../movie-bio
if [ -f movie-bio.mp4 ]; then
  rm movie-geo.mp4
fi

ffmpeg -f image2 -i movie-bio%d.png -vf scale="trunc(iw/2)*2:trunc(ih/2)*2" -c:v libx264 -profile:v high -pix_fmt yuv420p -g 20 -r 30 movie-bio.mp4

cd ..

if [ -d movies ];
then
  echo "movies directory exists!"
else
  mkdir movies
fi

mv movie-geo/movie-geo.mp4 movies
mv movie-bio/movie-bio.mp4 movies

# Sensitivity analysis
julia mads_syntheticdata.jl chrotran-bio report-bio 6 CrO4-- obs1
julia mads_syntheticdata.jl chrotran-geo report-geo 6 CrO4-- obs1
julia mads_syntheticdata.jl chrotran-rdx report-rdx 5 rdx well_injection

julia mads_writemadsfile.jl chrotran-bio report-bio
julia mads_writemadsfile.jl chrotran-geo report-geo
julia mads_writemadsfile.jl chrotran-rdx report-rdx

cd chrotran-bio/sensitivity
julia localsa.jl

cd ../../chrotran-geo/sensitivity
julia localsa.jl

cd ../../chrotran-rdx/sensitivity
julia localsa.jl

cd ../singleparameter
julia main.jl

# Miscellaneous simulation results: chrotran-geo global SA
cd ../../misc-figures
julia sensitivity_globalsa.jl
julia plot-obs.jl
