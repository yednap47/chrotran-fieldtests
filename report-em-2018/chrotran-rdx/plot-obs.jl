import Pflotran
import PyPlot
plt = PyPlot
using LaTeXStrings

# Initialize
mysize = 9 # font setting
obsnames = ["well_injection"] # observation name
nobsfile = [5] # number tagged onto the end of observation file name
mycol = ["blue","red","green","brown"] # color setting
varname = "Total rdx [M]"

basedir = "."
simnames = ["report-rdx",]
mylabels = ["test"]

f, ax = plt.subplots(figsize=(5.75,2.4))
for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["$varname $(obsnames[j])"]
        results = Pflotran.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->Pflotran.M2ppb(x,222.12),results[:,2])
        line = ax[:plot](x, y1,label="RDX",color = "b",ls="-")
        # line[:set_dashes](2)
    end
end
ax[:set_ylabel]("RDX [ppb]",size=mysize)
plt.xticks(fontsize=mysize)
plt.yticks(fontsize=mysize)
plt.ylim(0,30)

ax2 = ax[:twinx]() # Create another axis on top of the current axis
varname = "biomass [mol/m^3]"
for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["$varname $(obsnames[j])"]
        results = Pflotran.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = results[:,2]
        line = ax2[:plot](x, y1,label="biomass",color = "g",ls="-")
        # line[:set_dashes](2)
    end
end
plt.xticks(fontsize=mysize)
plt.yticks(fontsize=mysize)
plt.ylim(0,3.5)
plt.xlim(0,365)

# now plot control (no reaction)
filename = "report-rdx-norxn-obs-5.tec"
varname = "Total rdx [M]"
for j in 1:length(obsnames)
	myvar = ["$varname $(obsnames[j])"]
	results = Pflotran.readObsDataset(filename,myvar)
	x = results[:,1]
	y1 = map(x->Pflotran.M2ppb(x,222.12),results[:,2])
	line = ax[:plot](x, y1,label="dilution (no reaction)",color = "0.5",ls="--")
end

ax2[:set_ylabel]("Biomass, [g/m^3]",size=mysize)
# ax2[:set_ylim]((0,500))
ax[:set_xlabel]("Time [days]",size=mysize)

#ax[:set_xlim]((0,1))

handles, labels = ax2[:get_legend_handles_labels]()
handles2, labels2 = ax[:get_legend_handles_labels]() 
append!(handles,handles2)
append!(labels,labels2)
labelsandhandles = sort(collect(zip(labels, handles)), by=t->t[1])
ax2[:legend](map(x->x[2], labelsandhandles), map(x->x[1], labelsandhandles),loc=0,frameon=false,fontsize=mysize-2, handlelength=3)
plt.title("(C)",fontsize=mysize)
plt.tight_layout()

ax[:text](27,25,"Inject @ 10 gpm",fontsize=mysize-2)
ax[:plot]([10,25],[21,25], c= "k", lw = 0.5)
ax2[:text](62,2.3,"Pump @ 1 gpm",fontsize=mysize-2)
ax2[:plot]([35,60],[2.7,2.5], c= "k", lw = 0.5)

plt.savefig("rdx-breakthrough.png",dpi=600)
plt.close()
