import Mads
import ExcelReaders
xlr = ExcelReaders

function runforabit(command, timelimit, pollinterval=1)
    # kills terminal command if time limit is exceeded
    # note, all times are in seconds
    starttime = now()
    process = spawn(command)
    while !process_exited(process) && convert(Float64,Dates.value(now() - starttime)) / 1000 < timelimit
        sleep(pollinterval)
    end
    if !process_exited(process)
        kill(process)
        return false
    else
        return true
    end
end

# User info
sensparams = [
              "RATE_B_1",
              "RATE_B_2",
              "INHIBITION_C",
              ]

rundir =  "attempt1"
simbasename = "report-rdx"
nstops = 5 # nstops below, nstops above base

# pflotran information
pfle = "/lclscratch/sach/Programs/chrotran/release-2.0/src/pflotran/chrotran"
np = 16
maxruntime = 1 * 60 * 60 # seconds from hours

# make run directory
if !isdir(rundir)
    mkdir(rundir)
end

# read the mads file
madsdata = Mads.loadmadsfile("report-rdx.mads")

# get param names (non-log) and param ranges
logparams_init = Mads.getparamsinit(madsdata)
logparams_min = Mads.getparamsmin(madsdata)
logparams_max = Mads.getparamsmax(madsdata)
logparamkeys = Mads.getparamkeys(madsdata)
paramkeys = map(x->split(x,"log_")[2],logparamkeys)

# sensitivity analysis
for sensparam in sensparams
    # reinitialize paramkeys and params vals
    paramkeys_act = deepcopy(paramkeys)
    logparams_init_act = deepcopy(logparams_init)
    logparams_min_act = deepcopy(logparams_min)
    logparams_max_act = deepcopy(logparams_max)
    
    # make range for sensitivity analysis
    iparamloc = find(x -> x == sensparam,paramkeys)[1]


    # append param values below base value
    sensvals = Array{Float64}(0)
    smallparams = collect(linspace(logparams_min_act[iparamloc],logparams_init_act[iparamloc],nstops+1)[1:end-1])
    largeparams = collect(linspace(logparams_init_act[iparamloc],logparams_max_act[iparamloc],nstops+1)[2:end])
    sensvals = append!(smallparams,sensvals)
    sensvals = append!(sensvals,largeparams)
    sensvals = 10.^sensvals
    # @show sensvals 

    # make a matrix [parameters, sensitivity run]
    paramarray = Array{Float64}(length(paramkeys),length(sensvals))
    for i in 1:length(sensvals)
        paramarray[:,i] = 10.^logparams_init_act
        paramarray[iparamloc,i] = sensvals[i]
    end

    # now lets make the inputfiles
    if !isdir(joinpath(rundir,sensparam))
        mkdir(joinpath(rundir,sensparam))
    end

    for i in 1:length(sensvals)
        if !isdir(joinpath(rundir,sensparam,"run$i"))
            mkdir(joinpath(rundir,sensparam,"run$i"))
        end
        parameters = Dict(zip(paramkeys_act, paramarray[:,i]))
        templatefilename = "$simbasename.in.tpl"
        outputfilename = joinpath(rundir,sensparam,"run$i/$simbasename.in")
        Mads.writeparametersviatemplate(parameters, templatefilename, outputfilename)
        cd(joinpath(rundir,sensparam,"run$i"))
        try
            println("starting $sensparam run $i")
            asdf = pipeline(`mpirun -np $(np) $(pfle) -pflotranin $(simbasename).in`, "$(simbasename).txt")
            runforabit(asdf, maxruntime)
        catch
            warn("$(rundir)/$(sensparam)/run$(i) failed :()")
        end
        cd("../../..")
    end
end
