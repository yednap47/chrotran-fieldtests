import Pflotran
import PyPlot
plt = PyPlot
using LaTeXStrings

# Initialize
mysize = 9 # font setting
obsnames = ["well_injection"] # observation name
nobsfile = [5] # number tagged onto the end of observation file name
mycol = ["blue","red","green","brown"] # color setting
varname = "Total rdx [M]"

basedir = "attempt1"
simnames = ["report-rdx",]
mylabels = ["test"]

sensparams = [
              "RATE_B_1",
              "RATE_B_2",
              "INHIBITION_C"
              ]

coolnames = [L"\mathrm{\lambda_{B_1}}",L"\mathrm{\lambda_{B_2}}",L"\mathrm{K_C}"]

mycolors = ["g","b","r"]

nruns = 10

f, ax = plt.subplots(figsize=(5.75,2.5))
for iparam in 1:length(sensparams)
    sensparam = sensparams[iparam]
    ax[:plot]([0,0],[0,0],c=mycolors[iparam], label=coolnames[iparam])
    for irun in 1:nruns
        for i in 1:length(simnames)
            filename = joinpath(basedir,sensparam,"run$irun","$(simnames[i])-obs-$(nobsfile[i]).tec")
            for j in 1:length(obsnames)
                myvar = ["$varname $(obsnames[j])"]
                results = Pflotran.readObsDataset(filename,myvar)
                x = results[:,1]
                y1 = map(x->Pflotran.M2ppb(x,222.12),results[:,2])
                line = ax[:plot](x, y1, color=mycolors[iparam], ls="-")
                # line = ax[:plot](x, y1, label="RDX", color=mycolors[iparam], ls="-", dashes=(5, 2))
            end
        end
        ax[:set_ylabel]("RDX [ppb]",size=mysize)
        plt.xticks(fontsize=mysize)
        plt.yticks(fontsize=mysize)
    end
end

ax[:set_xlabel]("Time [days]",size=mysize)
plt.xlim(0,365)
plt.ylim(0,22)
plt.tight_layout()
box = ax[:get_position]()
ax[:set_position]([box[:x0], box[:y0], box[:width] * 0.85, box[:height]])
ax[:legend](fontsize=mysize,loc=4,bbox_to_anchor=(1.2, 0.6),frameon = false)

# ax[:annotate]("Test", color = "g",
#             xy=(300, 15), xycoords="data",
#             xytext=(325, 5), textcoords="data",
#             arrowprops=Dict("arrowstyle"=>"->",
#                             "connectionstyle"=>"arc3",
#                             "edgecolor"=>"g"),
#             )

# ax[:annotate](27,25,"Inject @ 10 gpm",fontsize=mysize-2)
# ax[:plot]([10,25],[21,25], c= "k", lw = 0.5)
# ax2[:text](62,2.3,"Pump @ 1 gpm",fontsize=mysize-2)
# ax2[:plot]([35,60],[2.7,2.5], c= "k", lw = 0.5)

plt.savefig("rdx-breakthrough-sens.png",dpi=600)
plt.close()
