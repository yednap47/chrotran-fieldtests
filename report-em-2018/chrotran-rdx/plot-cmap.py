import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
from numpy import ma
import h5py
from matplotlib.font_manager import FontProperties

filename = 'report-rdx.h5'
mysize = 8
timestep = 150
dt = 1.0
myvars = ['Total_rdx [M]','biomass [mol_m^3]','Liquid X-Velocity [m_per_d]','Liquid Y-Velocity [m_per_d]', 'Permeability_X [m^2]']

majorFormatter = plt.matplotlib.ticker.FormatStrFormatter("%0.0f")
MW_rdx = 222.12

f = h5py.File(filename, 'r')
keys = f.keys()
timekeys = [key for key in keys if 'Time:' in key]
times = [float(timekey.split('Time:  ')[1].split(' d')[0]) for timekey in timekeys]
st = sorted(times)
keydict = dict(zip(times,timekeys))

# get coordinates
xgrid = f['Coordinates']['X [m]'].value
ygrid = f['Coordinates']['Y [m]'].value
x, y = np.meshgrid(xgrid, ygrid)

results = dict()
for myvar in myvars:
	results[myvar] = f[keydict[timestep*dt]][myvar].value
	results[myvar] = results[myvar].reshape(len(xgrid)-1,len(ygrid)-1)

# RDX plot
plt.subplots(figsize = (5.75,2.25))
# plt.axis('scaled')
plt.pcolor(x, y, np.transpose(results['Total_rdx [M]'])*MW_rdx*10**6,vmin = 0.0,vmax = results['Total_rdx [M]'].max()*MW_rdx*10**6)
cbar = plt.colorbar()
cbar.set_label('RDX [ppb]',rotation=90,labelpad=20,fontsize=mysize)
plt.xlim(min(xgrid),max(xgrid))
plt.ylim(min(ygrid),max(ygrid))
cbar.ax.tick_params(labelsize=mysize) 

font0 = FontProperties()
font0.set_weight('bold')
plt.text(29.5,24.1,'x',fontsize=8,fontproperties=font0)

plt.xlim(0,100)
plt.ylim(0,50)
plt.xticks(fontsize=mysize)
plt.yticks(fontsize=mysize)
plt.xlabel('x [m]',size = mysize)
plt.ylabel('y [m]',size = mysize)
plt.title("(B)",fontsize=mysize)
plt.tight_layout()
plt.savefig('rdx-cmap-rdx-'+str(int(timestep*dt))+'d.png',dpi=600)
plt.close()

# Biomass plot
plt.subplots(figsize = (5.75,2.25))
# plt.axis('scaled')
plt.pcolor(x, y, np.transpose(results['biomass [mol_m^3]']),vmin = 0.0,vmax = results['biomass [mol_m^3]'].max())
cbar = plt.colorbar()
cbar.set_label('Biomass [mol/m^3]',rotation=90,labelpad=20,fontsize=mysize)
plt.xlim(min(xgrid),max(xgrid))
plt.ylim(min(ygrid),max(ygrid))
cbar.ax.tick_params(labelsize=mysize) 

plt.text(29.5,24.1,'x',fontsize=8,fontproperties=font0)

plt.xticks(fontsize=mysize)
plt.yticks(fontsize=mysize)
plt.xlabel('x [m]',size = mysize)
plt.ylabel('y [m]',size = mysize)
plt.title("(A)",fontsize=mysize)
plt.tight_layout()
plt.savefig('rdx-cmap-biomass-'+str(int(timestep*dt))+'d.png',dpi=600)
plt.close()

# Velocity plot
qsf = 1 # quiver plot skip factor
timestep = 9

f = h5py.File(filename, 'r')
keys = f.keys()
timekeys = [key for key in keys if 'Time:' in key]
times = [float(timekey.split('Time:  ')[1].split(' d')[0]) for timekey in timekeys]
st = sorted(times)
keydict = dict(zip(times,timekeys))

# get coordinates
xgrid = f['Coordinates']['X [m]'].value
ygrid = f['Coordinates']['Y [m]'].value
x, y = np.meshgrid(xgrid, ygrid)

results = dict()
for myvar in myvars:
	results[myvar] = f[keydict[timestep*dt]][myvar].value
	results[myvar] = results[myvar].reshape(len(xgrid)-1,len(ygrid)-1)
# K = plt.pcolor(x, y, np.transpose(results['Permeability_X [m^2]']),vmin = 0.0,vmax = results['Permeability_X [m^2]'].max())
plt.subplots(figsize = (4.75,3.75))
K = plt.pcolor(x, y, np.log10(np.transpose(results['Permeability_X [m^2]'])))
cbar = plt.colorbar()
cbar.set_label('log(k)',rotation=90,labelpad=20,fontsize=mysize)
cbar.ax.tick_params(labelsize=mysize) 
 
# plt.subplots(figsize = (5.75,2.25))
dxyz = xgrid[1]-xgrid[0]
x, y = np.mgrid[min(xgrid)+dxyz/2:max(xgrid)+dxyz/2:dxyz, min(ygrid)+dxyz/2:max(ygrid)+dxyz/2:dxyz]
Q = plt.quiver(x[::qsf,::qsf], y[::qsf,::qsf], results['Liquid X-Velocity [m_per_d]'][::qsf,::qsf], results['Liquid Y-Velocity [m_per_d]'][::qsf,::qsf],units='x',scale=0.019,angles='xy',headwidth=7,width=0.1)
plt.xlim(10,50)
plt.ylim(5,45)

font0 = FontProperties()
font0.set_weight('bold')

plt.text(29.9,25,'x',fontsize=12,fontproperties=font0)
plt.xticks(fontsize=mysize)
plt.yticks(fontsize=mysize)
plt.xlabel('x [m]',size = mysize)
plt.ylabel('y [m]',size = mysize)
plt.tight_layout()

# plt.show() 
plt.savefig('rdx-cmap-kfield-'+str(int(timestep*dt))+'d.png',dpi=600)
plt.close()


# RDX plot
