import Pflotran

myvar = ["7-Global rdx [mol]"]
fname_rxn = "report-rdx-mas.dat"
fname_norxn = "report-rdx-norxn-mas.dat"
time = 365
MW_RDX = 222.12

conc_rxn = Pflotran.readObsDataset(fname_rxn,myvar)[time,2]
conc_norxn = Pflotran.readObsDataset(fname_norxn,myvar)[time,2]
reduced = Dict()
reduced["moles"] = conc_norxn-conc_rxn
reduced["g"] = reduced["moles"] * MW_RDX
reduced["kg"] = reduced["g"]/1000
