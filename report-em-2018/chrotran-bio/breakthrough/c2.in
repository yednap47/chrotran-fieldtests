# Id: ct1di.in, Tue 5 Dec 2017 10:32:38 AM MDT pandeys #
# Created by Sachin Pandey, LANL
# Description: YES direct reaction YES inhibitor (i.e., alcohol)
#   - porewater velocity is 1.0 m/d (~ R-28)
#------------------------------------------------------------------------------

SIMULATION
  SIMULATION_TYPE SUBSURFACE
  PROCESS_MODELS
    SUBSURFACE_FLOW flow
      MODE RICHARDS
    /
    SUBSURFACE_TRANSPORT transport
      GLOBAL_IMPLICIT
      NUMERICAL_JACOBIAN
    /
  /
END

SUBSURFACE

#=========================== chemistry ========================================
CHEMISTRY
  PRIMARY_SPECIES
    molasses
    CrO4--
    ethanol
    biocide
  END
  IMMOBILE_SPECIES
    biomass
    molasses_im
  END
  MINERALS
    chubbite        # dummy mineral volume fraction = 0.85, porosity = 0.15
  END
  REACTION_SANDBOX
    CHROTRAN_PARAMETERS
       NAME_D_MOBILE        molasses
       NAME_D_IMMOBILE      molasses_im
       NAME_C               CrO4--
       NAME_B               biomass
       NAME_I               ethanol
       NAME_X               biocide
       NAME_BIOMINERAL      chubbite

       EXPONENT_B           1.0         # alpha [-]
       
       BACKGROUND_CONC_B    1.e-20      # B_min [mol/m^3_bulk]

       MASS_ACTION_B        0.d0        # Gamma_B [L/mol/s]
       MASS_ACTION_CD       1.0         # Gamma_CD [L/mol/s]
       MASS_ACTION_X        0.d0        # Gamma_X [L/mol/s]

       RATE_B_1             1.d-5       # lambda_B1 [/s]
       RATE_B_2             1.d-6       # lambda_B2 [/s]
       RATE_C               1.d-2       # lambda_C [/s]
       RATE_D               0.d0        # lambda_D [/s]
       RATE_D_IMMOB         150.d-2     # lambda_D_i [/s]
       RATE_D_MOBIL         1.d-2       # lambda_D_m [/s]
       
       INHIBITION_B         5.d1        # K_B [mol/m^3_bulk]
       INHIBITION_C         1.d-7       # K_C [M]
       MONOD_D              1.d-6       # K_D [M]
       INHIBITION_I         1.d-4       # K_I [M]
       
       DENSITY_B            1.d20       # [g/L = M]
       
       STOICHIOMETRIC_C     0.33d0      # S_C [-]
       STOICHIOMETRIC_D_1   1.d0        # S_D_1 [-]
       STOICHIOMETRIC_D_2   0.020833d0  # S_D_2 [-]
    END
  END
  MINERAL_KINETICS
    chubbite 
        RATE_CONSTANT 0.d0
    END
  END
  UPDATE_POROSITY
  MINIMUM_POROSITY 1.d-4
  UPDATE_PERMEABILITY
  UPDATE_MINERAL_SURFACE_AREA
  DATABASE ../../chromium.dat
  OUTPUT
    ALL
    TOTAL
  /
  LOG_FORMULATION
END

#=========================== water options =+==================================
EOS WATER
  DENSITY CONSTANT 999.10d0 kg/m^3 # 15 deg C
  VISCOSITY CONSTANT 0.0010016d0 # Pa - s (i.e. dynamic viscocity)
END

#=========================== solver options ===================================
TIMESTEPPER FLOW
  TS_ACCELERATION 8
END

NEWTON_SOLVER TRANSPORT
  STOL 1.d-30
  ITOL 1.d-8
  RTOL 1.d-8
  MAXIT 25
END

#=========================== discretization ===================================
GRID
  TYPE structured
  NXYZ 600 150 1
  BOUNDS
    0.0 0.0 0.0
    600.0 150.0 30.0
  /
END

#============================== dataset =======================================
DATASET Permeability
  FILENAME ../../domains/rand_600x150_corr10_sig1_v2_20mpd.h5
END

#=========================== fluid properties =================================
FLUID_PROPERTY
  DIFFUSION_COEFFICIENT 1.d-9
END

#=========================== material properties ==============================
MATERIAL_PROPERTY soil1
  ID 1
  POROSITY 0.15d0
  TORTUOSITY 1.d0
  SATURATION_FUNCTION cc1
  ROCK_DENSITY 1200.d0 # kg/m^3_bulk
  PERMEABILITY
    DATASET Permeability
  /
#  LONGITUDINAL_DISPERSIVITY 5.d0
END

#=========================== characteristic curves ============================
CHARACTERISTIC_CURVES cc1
  SATURATION_FUNCTION VAN_GENUCHTEN
    ALPHA  1.d-4
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
  PERMEABILITY_FUNCTION MUALEM_VG_LIQ
    M 0.5d0
    LIQUID_RESIDUAL_SATURATION 0.1d0
  /
END

#=========================== output options ===================================
OUTPUT
#  FORMAT TECPLOT BLOCK
  FORMAT HDF5
  VELOCITY_AT_CENTER
  VARIABLES
    LIQUID_SATURATION
    LIQUID_PRESSURE
    LIQUID_HEAD 
    PERMEABILITY
  END
  PERIODIC TIME 1.d0 d
  PERIODIC_OBSERVATION TIME 1.d0 d
  PRINT_COLUMN_IDS
#  MASS_BALANCE_FILE
#    PERIODIC TIME  1.d0 d
#  /
END

#=========================== times ============================================
TIME
  FINAL_TIME 365.d0 d
  INITIAL_TIMESTEP_SIZE 1.d-12 y 
  MAXIMUM_TIMESTEP_SIZE 1.d0 d at 0.0 d
  MAXIMUM_TIMESTEP_SIZE 1.d-4 d at 10.0 d
  MAXIMUM_TIMESTEP_SIZE 1.d-1 d at 10.0001 d
  MAXIMUM_TIMESTEP_SIZE 1.d0 d at 30.0 d
END

#=========================== regions ==========================================
REGION all
  COORDINATES
     0.0 0.0 0.0
     600.0 150.0 30.0
  /
END

REGION west
  FACE WEST
  COORDINATES
     0.0 0.0 0.0
     0.0 150.0 30.0
  /
END

REGION east
  FACE EAST
  COORDINATES
     600.0 0.0 0.0
     600.0 150.0 30.0
  /
END

REGION south
  FACE SOUTH
  COORDINATES
     0.0 0.0 0.0
     600.0 0.0 30.0
  /
END

REGION north
  FACE NORTH
  COORDINATES
     0.0 150.0 0.0
     600.0 150.0 30.0
  /
END

REGION well_injection
  COORDINATES
    40.50 75.50 0.0
    40.50 75.50 30.0
  /
END

REGION obs1
  COORDINATES
  50.50 75.5 0.0
  50.50 75.5 30.0
  /
END

REGION obs2
  COORDINATES
  60.50 75.5 0.0
  60.50 75.5 30.0
  /
END

REGION obs3
  COORDINATES
  70.50 75.5 0.0
  70.50 75.5 30.0
  /
END

REGION obs4
  COORDINATES
  80.50 75.5 0.0
  80.50 75.5 30.0
  /
END

#================== observation points ========================
OBSERVATION obs1
  REGION obs1
/

OBSERVATION obs2
  REGION obs2
/

OBSERVATION obs3
  REGION obs3
/

OBSERVATION obs4
  REGION obs4
/

OBSERVATION well_injection
  REGION well_injection
/

#=========================== flow conditions ==================================
FLOW_CONDITION initial
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.78924583e+06
END

FLOW_CONDITION no_flow
  TYPE
    FLUX neumann
  /
  FLUX 0.d0
END

FLOW_CONDITION west
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.78924583e+06
END

FLOW_CONDITION east
  TYPE
    PRESSURE hydrostatic
  /
  DATUM 0.d0 0.d0 0.d0
  PRESSURE 9.74519422e+06
END

FLOW_CONDITION well
  TYPE
    RATE mass_rate
  /
  RATE list
    TIME_UNITS d
    DATA_UNITS kg/s
    0.0 0.0
    10.0 0.6298
    30.0 0.0
  /
END

#=========================== transport conditions =============================
TRANSPORT_CONDITION initial
  TYPE dirichlet
  CONSTRAINT_LIST
    0.d0 initial
  /
END

TRANSPORT_CONDITION inlet
  TYPE dirichlet_zero_gradient
  CONSTRAINT_LIST
    0.d0 inlet
  /
END

TRANSPORT_CONDITION outlet
  TYPE dirichlet_zero_gradient
  CONSTRAINT_LIST
    0.d0 inlet
  /
END

TRANSPORT_CONDITION injectant
  TYPE dirichlet
  CONSTRAINT_LIST
    0.d0 injectant
  /
END

#=========================== constraints ======================================
CONSTRAINT initial
  CONCENTRATIONS
    molasses    1.d-20 T
    ethanol     1.d-20 T
    biocide     1.d-20 T
    CrO4--      1.9200e-05 T
  END
  IMMOBILE
    biomass     1.d-20
    molasses_im 1.d-20
  END
  MINERALS
    chubbite    0.85 1.0    # 0.15 is initial porosity
  END
END

CONSTRAINT inlet
  CONCENTRATIONS
    molasses    1.d-20 T
    ethanol     1.d-20 T
    biocide     1.d-20 T
    CrO4--      1.9200e-05 T
  END
END

CONSTRAINT injectant
  CONCENTRATIONS
    molasses    1.d-2 T
    ethanol     1.d0 T
    biocide     1.d-20 T
    CrO4--      1.d-20 T
  END
END

#=========================== condition couplers ===============================
# initial condition
INITIAL_CONDITION
  TRANSPORT_CONDITION initial
  FLOW_CONDITION initial
  REGION all
END

BOUNDARY_CONDITION west
  TRANSPORT_CONDITION inlet
  FLOW_CONDITION west 
  REGION west 
END

BOUNDARY_CONDITION east  
  TRANSPORT_CONDITION outlet
  FLOW_CONDITION east 
  REGION east
END

SOURCE_SINK
  TRANSPORT_CONDITION injectant
  FLOW_CONDITION well
  REGION well_injection
END

#=========================== stratigraphy couplers ============================
STRATA
  REGION all 
  MATERIAL soil1
END

END_SUBSURFACE
