import(Mads)
import PyPlot
plt = PyPlot
import JLD
using LaTeXStrings

tic()
# Load mads file, set weights
md = Mads.loadmadsfile("report-bio.mads")

# Check to make sure that the parameters are correct
o = Mads.forward(md)
Mads.plotmatches(md, o, r"obs", filename="init.png")

# Local SA
info("Local sensitivity analysis for known parameters")
Mads.localsa(md, filename="localsa.png", datafiles=true)

toc()

# Manually plot local sa results for paper
mysize = 9
include("coolnames.jl")
eigenmat_raw = readdlm("localsa-eigenmatrix.dat")

eigenmat_names = eigenmat_raw[:,1]
for i in 1:length(eigenmat_names)
    eigenmat_names[i] = coolnames[eigenmat_names[i]]
end

f, ax = plt.subplots(figsize=(4,3))

eigenmat_clean = eigenmat_raw[:,2:end]
eigenmat_clean = map(x->Float64(x),eigenmat_clean)
plt.pcolor(eigenmat_clean[end:-1:1,1:1:end],cmap="RdBu_r",vmin=-1, vmax=1)
cbar = plt.colorbar()
plt.xlim(0,length(eigenmat_names))
plt.ylim(0,length(eigenmat_names))
plt.xlabel("Eigenvectors",fontsize=mysize)
plt.ylabel("Parameters",fontsize=mysize)
plt.xticks(0.5:1:7.5,("1","2","3","4","5","6","7","8"))
plt.yticks(0.5:1:7.5,eigenmat_names[end:-1:1])

plt.tick_params(axis="x", which="both", bottom="off", top="off")
plt.tick_params(axis="y", which="both", left="off", right="off")
ax[:tick_params](labelsize=mysize)
cbarvalues = -1.0:.2:1.0
cbarvalues = [LaTeXStrings.LaTeXString("\$$cbarvalue\$") for cbarvalue in cbarvalues]

cbar[:ax][:set_yticklabels](cbarvalues)
cbar[:ax][:tick_params](labelsize=mysize-2)

plt.tight_layout()
plt.draw()
plt.savefig("localsa-chrotran-bio.png",dpi=600)
plt.close()
