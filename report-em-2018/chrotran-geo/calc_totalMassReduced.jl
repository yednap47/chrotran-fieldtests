import Pflotran

myvar = ["7-Global CrO4-- [mol]"]
fname_rxn = "report-geo-mas.dat"
fname_norxn = "report-geo-norxn-mas.dat"
time = 365 
MW_Cr = 52.00
MW_CrO4 = 115.99

conc_rxn = Pflotran.readObsDataset(fname_rxn,myvar)[time,2]
conc_norxn = Pflotran.readObsDataset(fname_norxn,myvar)[time,2]
reduced = Dict()
reduced["moles"] = conc_norxn-conc_rxn
reduced["g"] = reduced["moles"] * MW_Cr
reduced["kg"] = reduced["g"]/1000
