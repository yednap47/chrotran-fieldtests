import Pflotran

casetag = "report-geo" # for debug
nametag = "movie_geo" # for debug
mytime = 365
h5varname = ["Cr(OH)3(s)_VF"]
vcell = 1*1*30 # m^3
MW_Cr = 52.0
MV_CrOH3 = 33.1 * 1e-6 # m^3/mol

filename = "$(casetag).h5"
results = Pflotran.readh5_2D(filename,h5varname,mytime)
croh3_mole = results["Cr(OH)3(s)_VF"] * vcell / MV_CrOH3 # moles
croh3_g = croh3_mole * MW_Cr # grams
total_croh3_g = sum(croh3_g) # grams
total_croh3_kg = total_croh3_g/1000 # kg
