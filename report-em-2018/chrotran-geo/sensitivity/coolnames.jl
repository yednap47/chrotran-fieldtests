import DataStructures

using LaTeXStrings

coolnames  = DataStructures.OrderedDict{AbstractString,LaTeXStrings.LaTeXString}(
"log_K_S2O4_DISP"=>L"\mathrm{k_{S_2O_4^{-2}-disp}}",
"log_K_S2O4_O2"=>L"\mathrm{k_{S_2O_4^{-2}-O_2(aq)}}",
"log_K_S2O4_FE3"=>L"\mathrm{k_{S_2O_4^{-2}-Fe(OH)_3(s)}}",
"log_K_FE2_O2_FAST"=>L"\mathrm{k_{\equiv Fe(II)-O_2(aq)}}",
"log_K_FE2_CR6_FAST"=>L"\mathrm{k_{\equiv Fe(II)-HCrO_4^-}}",
"log_factor_k_fe2_cr6_slow"=>L"\mathrm{\beta_{\equiv Fe(II)-HCrO_4^-}}",
"log_is2o4"=>L"\mathrm{[S_2O_4^{-2}]}",
"log_IFEOH3"=>L"\mathrm{wt\%\,Fe(OH)_3(s)}")
