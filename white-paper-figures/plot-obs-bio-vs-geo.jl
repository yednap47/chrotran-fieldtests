import sachConvert
import sachFun
import PyPlot
plt = PyPlot
using LaTeXStrings

# Initialize
mysize = 11
obsnames = ["obs2"]
nobsfile = [6,6,6,6]
mycol = ["blue","red","green","brown"]

# first plot the bio simulations
basedir = "chrotran-bio/example_1"
simnames = ["CTdi","CTdx","CTxi","CTxx"]
mylabels = [L"\mathrm{direct,\, ethanol}",
            L"\mathrm{direct,\, no\, ethanol}",
            L"\mathrm{no\, direct,\, ethanol}",
            L"\mathrm{no\, direct,\, no\, ethanol}"]


f, ax = plt.subplots(figsize=(8.0,3.5))
for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["Total Cr(VI) [M] $(obsnames[j])"]
        results = sachFun.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->sachConvert.M2ppb(x,51.9961),results[:,2])
        line = ax[:plot](x/365, y1,label=mylabels[i],color = mycol[i],ls="--",dashes=(5, 2))
        # line[:set_dashes](2)
    end
end

# now plot the geo simulations
basedir = "chrotran-geo/example_1"
simnames = ["s1","s2","s3","s4"]
mylabels = [L"\mathrm{10^{-1}\, M\, dithionite}",
           L"\mathrm{10^{-2}\, M\, dithionite}",
           L"\mathrm{10^{-3}\, M\, dithionite}",
           L"\mathrm{10^{-4}\, M\, dithionite}",
           ]
nobsfile = [6,6,6,12]

for i in 1:length(simnames)
    filename = joinpath(basedir,"$(simnames[i])-obs-$(nobsfile[i]).tec")
    for j in 1:length(obsnames)
        myvar = ["Total CrO4-- [M] $(obsnames[j])"]
        results = sachFun.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->sachConvert.M2ppb(x,51.9961),results[:,2])
        # y1[y1.>1000]=1000
        line = ax[:plot](x/365, y1,label=mylabels[i],color = mycol[i],ls="-")
    end
end

new_position = [0.1,0.15,.67,.8] # Position Method 2
ax[:set_position](new_position) # Position Method 2: Change the size and position of the axis
ax[:tick_params](labelsize=mysize)
ax[:set_xlabel](L"\mathrm{Time\, [year]}",size=mysize+2)
ax[:set_ylabel](L"\mathrm{Cr(VI),\, [ppb]}",size=mysize+2)
ax[:set_xlim]((0,1))

# plt.legend(loc=0,frameon=false,fontsize=mysize-2)
ax[:legend](loc=4, bbox_to_anchor=(1.32, 0.0),fontsize=mysize-2,handlelength=2,frameon=false)

# tight_layout(h_pad=.1)
plt.savefig("breakthrough_biogeo.png",dpi=600)
close()
