#!/bin/bash

mpirun -np 8 $pfle_dithionite -pflotranin 1d-allReactions-10m-checkpoint.in
mpirun -np 8 $pfle_dithionite -pflotranin 1d-allReactions-10m.in
mpirun -np 8 $pfle_dithionite -pflotranin s1.in
mpirun -np 8 $pfle_dithionite -pflotranin s2.in
mpirun -np 8 $pfle_dithionite -pflotranin s3.in
mpirun -np 8 $pfle_dithionite -pflotranin s4.in
mpirun -np 8 $pfle_dithionite -pflotranin s5.in

julia plot_CvsT.jl
julia plot_CvsX.jl
