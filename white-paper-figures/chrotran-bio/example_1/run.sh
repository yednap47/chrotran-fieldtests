#!/bin/bash

mpirun -np 16 /lclscratch/sach/Programs/chrotran/release/src/pflotran/chrotran -pflotranin CTdi.in 2>&1 | tee CTdi.txt
mpirun -np 16 /lclscratch/sach/Programs/chrotran/release/src/pflotran/chrotran -pflotranin CTdx.in 2>&1 | tee CTdx.txt
mpirun -np 16 /lclscratch/sach/Programs/chrotran/release/src/pflotran/chrotran -pflotranin CTxi.in 2>&1 | tee CTxi.txt
mpirun -np 16 /lclscratch/sach/Programs/chrotran/release/src/pflotran/chrotran -pflotranin CTxx.in 2>&1 | tee CTxx.txt
