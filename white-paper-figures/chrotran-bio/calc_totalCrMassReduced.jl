import sachFun

myvar = ["6-Global Cr(VI) [mol]"]
fname_norxn = "white-paper-bio-1mpd-norxn-mas.dat"
fname_rxn = "white-paper-bio-1mpd-mas.dat"
time = 20
MW_Cr = 52.00

conc_rxn = sachFun.readObsDataset(fname_rxn,myvar)[time,2]
conc_norxn = sachFun.readObsDataset(fname_norxn,myvar)[time,2]
reduced = Dict()
reduced["moles"] = conc_norxn-conc_rxn
reduced["g"] = reduced["moles"] * MW_Cr
# reduced["kg"] = reduced["g"]/1000
