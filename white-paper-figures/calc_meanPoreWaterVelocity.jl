import sachFun
sf = sachFun
import DataFrames
df = DataFrames

basedir = "/lclscratch/sach/white-paper-figures"

# simdir = "./chrotran-geo/2d_heterogeneous_1yr"
# casetag = "white-paper-geo"

simdir = "./chrotran-bio/2d_heterogeneous_1yr"
casetag = "white-paper-bio"

myvar = ["Liquid X-Velocity [m_per_d]","Liquid Y-Velocity [m_per_d]","Liquid Z-Velocity [m_per_d]"]
mytime = 100
phi = 0.15

# get the results
filename = "$(casetag).h5"
cd(joinpath(basedir,simdir))
resultsdf = sf.readh5_2D(filename,myvar,mytime,dataframe=true)
cd(basedir)

# calculate magnitude of velocity vector in each cell
nrows, ncols = df.size(resultsdf)
velocitymag = Array{Float64}(nrows)
for i in 1:nrows
    velocitymag[i] = sqrt(sum([resultsdf[i,Symbol(var)]^2 for var in myvar]))
end

q_avg = mean(velocitymag)
v_avg = q_avg/0.15
