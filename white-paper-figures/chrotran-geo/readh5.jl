import HDF5
import PyPlot
plt = PyPlot

# ---------------------------------------------------------
# User specified
# ---------------------------------------------------------
# casetag = "2d_heterogeneous"
# ntime = 100 

casetag = "white-paper-geo-1mpd"
ntime = 365

nx = 125
ny = 75
dxyz = 1
well = [40.50,37.50]
aqSpec = "Total_CrO4-- [M]"
MW_Cr = 52.0
mcl = 50

# ---------------------------------------------------------
# The code
# ---------------------------------------------------------
htag = casetag * ".h5"
fid = HDF5.h5open(htag,"r")
h5dict = HDF5.read(fid)
HDF5.close(fid)

keyarray = collect(keys(h5dict))
timekeyarray = filter(key -> split(key)[1] == "Time:", keyarray)
timearray = [parse(Float64,split(key)[2]) for key in timekeyarray]
sta = sort(timearray)

keydict = Dict(zip(timearray,timekeyarray))

results = reshape(h5dict[keydict[sta[ntime]]][aqSpec][:],ny,nx)*MW_Cr*10^6

# ---------------------------------------------------------
# Plotting
# ---------------------------------------------------------
fig, ax = plt.subplots()
xgrid = 1:nx*dxyz
ygrid = 1:ny*dxyz
ax[:plot](well[1],well[2],color="black",marker="x",label="Water Level",markersize = 12,mew=2)
# cp = ax[:contour](xgrid, ygrid, results, colors="black", linewidth=2.0)
# cp = ax[:contour](xgrid, ygrid, results, linewidth=2.0)
# cp = ax[:plot_surface](xgrid, ygrid, results)
cp = plt.pcolor(xgrid, ygrid, mcl-results)
# ax[:clabel](cp, inline=1, fontsize=10)
# ax[:set_ylabel]("y, meters")
# ax[:set_xlabel]("x, meters")
ax[:set_xlim](1,nx*dxyz)
ax[:set_ylim](1,ny*dxyz)
plt.colorbar()

plt.show()

results_vector = Array(Float64,0)
xs = Array(Float64,0)
ys = Array(Float64,0)
for i in 1:length(xgrid)
    for j in 1:length(ygrid)
        append!(results_vector,results[j,i])
        append!(xs,xgrid[i])
        append!(ys,ygrid[j])
    end
end
