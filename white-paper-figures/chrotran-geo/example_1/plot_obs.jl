import sachConvert
import sachFun
import PyPlot
plt = PyPlot

simnames = ["s1","s2","s3","s4"]
obsnames = ["obs1","obs2","obs3","obs4"]
nobsfile = [6,6,6,6]
mycol = ["blue","red","green","brown"]

f, axarray = plt.subplots(2, 2, figsize=(10,10))
for i in 1:length(simnames)
    filename = "$(simnames[i])-obs-$(nobsfile[i]).tec"
    for j in 1:length(obsnames)
        myvar = ["Total CrO4-- [M] $(obsnames[j])"]
        results = sachFun.readObsDataset(filename,myvar)
        x = results[:,1]
        y1 = map(x->sachConvert.M2ppb(x,51.9961),results[:,2])
        axarray[i][:plot](x, y1,label=obsnames[j],color = mycol[j])
        axarray[i][:set_xlabel]("Time, year")
        axarray[i][:set_ylim]((0,1000))
        axarray[i][:set_ylabel]("Cr(VI), ug/l")
        axarray[i][:set_title]("$(simnames[i])")
    end
end

axarray[4][:legend](loc=0,frameon=false)

# 
# savefig("$(fbasename).png")
# close()
