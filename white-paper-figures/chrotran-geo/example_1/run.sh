#!/bin/bash

mpirun -np 16 /lclscratch/sach/Programs/pflotran-dithionite/src/pflotran/pflotran -pflotranin s1.in 2>&1 | tee s1.txt
mpirun -np 16 /lclscratch/sach/Programs/pflotran-dithionite/src/pflotran/pflotran -pflotranin s2.in 2>&1 | tee s2.txt
mpirun -np 16 /lclscratch/sach/Programs/pflotran-dithionite/src/pflotran/pflotran -pflotranin s3.in 2>&1 | tee s3.txt
mpirun -np 16 /lclscratch/sach/Programs/pflotran-dithionite/src/pflotran/pflotran -pflotranin s4.in 2>&1 | tee s4.txt
