import sachFun

myvar = ["8-Global CrO4-- [mol]"]
fname_norxn = "white-paper-geo-0p1mpd-norxn-mas.dat"
fname_rxn = "white-paper-geo-0p1mpd-mas.dat"
time = 230
MW_Cr = 52.00
MW_CrO4 = 115.99

conc_rxn = sachFun.readObsDataset(fname_rxn,myvar)[time,2]
conc_norxn = sachFun.readObsDataset(fname_norxn,myvar)[time,2]
reduced = Dict()
reduced["moles"] = conc_norxn-conc_rxn
reduced["g"] = reduced["moles"] * MW_Cr
# reduced["kg"] = reduced["g"]/1000
