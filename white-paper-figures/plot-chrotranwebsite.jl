import CrPlots
import sachFun
sf = sachFun
import DataFrames
df = DataFrames

# ---------------------------------------------------------
# User specified
# ---------------------------------------------------------
casetag = "white-paper-bio-1mpd"
basedir = pwd()
simdir = "./chrotran-bio"
mytime = 365
wellnames = ["R-28","CrIN-1"]

coolnames = ["Well #1","Well #2"]
injectionwell = "R-28"
h5varname = ["Total_Cr(VI) [M]"]
well_coordsim = [40.50,75.50]

MW_Cr = 52.0
mcl = 50
cbackground = 998.3999924964252

rainbow = PyPlot.matplotlib[:cm][:rainbow]

# gist_ncar = PyPlot.matplotlib[:cm][:gist_ncar]

# ---------------------------------------------------------
# The code
# ---------------------------------------------------------
# get the results
filename = "$(casetag).h5"
cd(joinpath(basedir,simdir))
results = sf.readh5_2D(filename,h5varname,mytime)
cd(basedir)

# get well information
welllocations = Array{Float64}(2, length(wellnames))
for (i, wellname) in enumerate(wellnames)
	x, y = CrPlots.getwelllocation(wellname)
	welllocations[1, i] = x
	welllocations[2, i] = y
end
xs = welllocations[1, :]
ys = welllocations[2, :]

# shift coordinates
iinjectionwell = find(x -> x == injectionwell,wellnames)[1]
well_coordact = [xs[iinjectionwell],ys[iinjectionwell]]
x_correction = well_coordact[1] - well_coordsim[1]
y_correction = well_coordact[2] - well_coordsim[2]

x_grid_2d =  results["xgrid"]
y_grid_2d = results["ygrid"]

x_vector_corrected = x_grid_2d + x_correction
y_vector_corrected = y_grid_2d + y_correction

# ---------------------------------------------------------
# Plotting
# ---------------------------------------------------------
boundingbox = (minimum(xs) - 100, minimum(ys), maximum(xs) + 50, maximum(ys))
maxval = 0.4
threshold = 0.2

fig, ax, img = CrPlots.crplot(boundingbox, x_vector_corrected, y_vector_corrected, (cbackground-results[h5varname[1]]*MW_Cr*10^6), cmap = rainbow, lowerlimit=1)

CrPlots.addwells(ax, wellnames; coolnames=coolnames)
CrPlots.addcbar(fig, img,  "Cr(VI) removed", [0 200 400 600 800 1000],cbar_x0=0.07, cbar_y0=0.4, cbar_height=0.25)
fig[:savefig]("chrotran-github.png")
PyPlot.close(fig)
