using sachFun
using HDF5
using PyPlot

fig = figure("pyplot",figsize=(3.25,3))
filename = "1d-allReactions-10m"

myvar = [
"fast_Fe++ [mol_m^3]",
]

coord_name = "X"
# mytime = [1,2,3,4,5]
mytime = [0.1,0.3,0.5,0.7,0.9]
# mytime = [0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08]
timeunits = "years"
mysize = 11

# mylabel = ["$(time) $(timeunits)" for time in mytime]
mylabel =  [L"0.1\; \mathrm{years}",
            L"0.3\; \mathrm{years}",
            L"0.5\; \mathrm{years}",
            L"0.7\; \mathrm{years}",
            L"0.9\; \mathrm{years}"
            ]
mycmap = get_cmap("Paired",length(mytime)+1)

majorFormatter = matplotlib[:ticker][:FormatStrFormatter]("%0.0f")
i=1
  for j in 1:length(mytime)
    results = sachFun.readh5_1D("$(filename).h5",[myvar[i]],coord_name,mytime[j])
    ax = gca()
    new_position = [0.21,0.15,.75,.8] # Position Method 2
    ax[:set_position](new_position) # Position Method 2: Change the size and position of the axis
    plot(results[:,1],(results[:,2]),label=mylabel[j],c=mycmap(j))
    ax[:tick_params](labelsize=mysize)
    ax[:set_xlabel](L"\mathrm{Distance\; from\; inlet\;[m]}",size=mysize+2)
    ax[:yaxis][:set_major_formatter](majorFormatter)
    ax[:set_ylabel](L"\mathrm{Fe(II),\; mol\;m^{-3}}",size=mysize+2)  
    ax[:set_xlim]([0,maximum(results[:,1])])
    ax[:set_xlim]([0,5])
    # ax[:set_ylim]([0,maximum(results[:,2])])
  end

legend(loc=0,frameon=false,fontsize=mysize-2)
savefig("1d-allReactions-10m-CvsX.png",dpi=600)
close()
