import CrPlots
import sachFun
sf = sachFun
import DataFrames
df = DataFrames
import PyPlot
plt = PyPlot

# ---------------------------------------------------------
# User specified
# ---------------------------------------------------------
# julia movie-topo-cmap-bio.jl white-paper-geo-0p1mpd biomovie
# casetag = ARGS[1]
# nametag = ARGS[2]
casetag = "white-paper-geo-0p1mpd" # for debug
nametag = "movie_topo_geo" # for debug
basedir = "/lclscratch/sach/chrotran-fieldtests/white-paper-figures/"
simdir = "./chrotran-geo"
mytimes = 12:1:365
t0 = 11
wellnames = ["R-42","CrPZ-3"]
coolnames = ["Well #1", "Well #2"]
injectionwell = "CrPZ-3"
h5varname = ["Total_CrO4-- [M]"]
obsvarname = ["8-Global CrO4-- [mol]"]
boxoffset = [75,50,100,50] # bounding box offset
well_coordsim = [40.50,75.50] # injection well simulation coordinates
MW_Cr = 52.0
cbackground = 998.3999924964252 # to calculate delta_Cr
mysize = 15 # font size for mini plot
meterheight = 5

# Topography image and coordinates
const bgimg = plt.matplotlib[:image][:imread]("/home/sach/JuliaModules/CrPlots.jl/data/bghuge.png")
const bgx0 = 496278.281759
const bgy0 = 537396.470881
const bgx1 = 501949.141159
const bgy1 = 541047.928481

# ---------------------------------------------------------
# The code
# ---------------------------------------------------------
mycmap = PyPlot.matplotlib[:cm][:rainbow]

if !isdir(nametag)
    mkdir(nametag)
end

# First, read the observation files
cd(joinpath(basedir,simdir))
obstimes = sachFun.readObsDataset("./$(casetag)-mas.dat",obsvarname)[:,1] 
conc_rxn = sachFun.readObsDataset("./$(casetag)-mas.dat",obsvarname)[:,2]
conc_norxn = sachFun.readObsDataset("./$(casetag)-norxn-mas.dat",obsvarname)[:,2]
reduced = Dict()
reduced["moles"] = conc_norxn-conc_rxn
reduced["g"] = reduced["moles"] * MW_Cr
reduced["kg"] = reduced["g"]/1000
cd(basedir)

for mytime in mytimes
    println("day $mytime")

    cd(joinpath(basedir,simdir))
    # get the 2D results
    filename = "$(casetag).h5"
    results = sf.readh5_2D(filename,h5varname,mytime)
    cd(basedir)

    # get well information
    welllocations = Array{Float64}(2, length(wellnames))
    for (i, wellname) in enumerate(wellnames)
        x, y = CrPlots.getwelllocation(wellname)
        welllocations[1, i] = x
        welllocations[2, i] = y
    end
    xs = welllocations[1, :]
    ys = welllocations[2, :]

    # shift coordinates
    iinjectionwell = find(x -> x == injectionwell,wellnames)[1]
    well_coordact = [xs[iinjectionwell],ys[iinjectionwell]]
    x_correction = well_coordact[1] - well_coordsim[1]
    y_correction = well_coordact[2] - well_coordsim[2]

    x_grid_2d =  results["xgrid"]
    y_grid_2d = results["ygrid"]

    x_vector_corrected = x_grid_2d + x_correction
    y_vector_corrected = y_grid_2d + y_correction

    # ---------------------------------------------------------
    # Plotting
    # ---------------------------------------------------------
    boundingbox = (minimum(xs) - boxoffset[1], minimum(ys) - boxoffset[2], maximum(xs) + boxoffset[3], maximum(ys) + boxoffset[4])
    maxval = 0.4
    threshold = 0.2

    boundingbox = CrPlots.resizeboundingbox(boundingbox)
    x0, y0, x1, y1 = boundingbox
    fig, ax = plt.subplots()
    fig[:delaxes](ax)
    ax = fig[:add_axes]([0, 0, 1, 1], frameon=false)
    # ax = fig[:add_axes]([-0.1, 0, 0.75, 1], frameon=false)
    fig[:set_size_inches](16, 9)
    img = ax[:imshow](bgimg, extent=[bgx0, bgx1, bgy0, bgy1], alpha=1.)
    ax[:axis]("off")
    ax[:set_xlim](x0, x1)
    ax[:set_ylim](y0, y1)

    figax = [fig ax]

    fig, ax, img = CrPlots.crplot(boundingbox, x_vector_corrected, y_vector_corrected, (cbackground-results[h5varname[1]]*MW_Cr*10^6), cmap = mycmap, lowerlimit=1,figax = figax)
    CrPlots.addwells(ax, wellnames, fontsize = 14; coolnames=coolnames)
    CrPlots.addmeter(ax, boundingbox[1] + 50, boundingbox[2] - 100, [250, 500, 1000], ["250m", "500m", "1km"])
    CrPlots.addcbar(fig, img,  "Cr(VI) removed \n (ppb)", [0 200 400 600 800 1000],cbar_x0=0.05, cbar_y0=0.5)
    CrPlots.addpbar(fig, ax, (mytime-t0)/(mytimes[end]-t0), "$(mytime-t0) day"; pbar_x0 = 0.75, pbar_y0=0.9)
    CrPlots.addmeter(ax, boundingbox[1] + 25, boundingbox[2] + 25, [15, 45, 90], ["15m", "45m", "90m"],textoffsety=7, meterheight=meterheight)

    ax2 = fig[:add_axes]([0.75, 0.1, 0.2, 0.25], frameon=true)
    ax2[:plot](obstimes,reduced["g"],linewidth=2)
    ax2[:scatter](obstimes[mytime],reduced["g"][mytime],s=100)
    ax2[:set_xlabel]("Time [day]",size=mysize)
    ax2[:set_ylabel]("Chromium reduced [g]",size=mysize)
    ax2[:set_xlim](0,375)
    ax2[:set_ylim](0,maximum(reduced["g"])+50)
    ax2[:tick_params](labelsize=mysize)

    fig[:savefig]("$(nametag)/$(nametag)$(mytime-t0).png")
    plt.close(fig)
end
